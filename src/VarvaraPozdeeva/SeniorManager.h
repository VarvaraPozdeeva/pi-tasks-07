#pragma once
#include "ProjectManager.h"
class SeniorManager: public ProjectManager
{
public:
	SeniorManager() {}
	SeniorManager(int id, string fio, int num, int pay, double persent, string proj, int budget)
	{
		this->ID = id;
		this->Fio = fio;
		this->num = num;
		this->payForOne = pay;
		this->persent = persent;
		this->project = proj;
		this->budget = budget;
	}
	string Outspec()
	{
		return "| SeniorManager  |";
	}
	~SeniorManager() {}
};

