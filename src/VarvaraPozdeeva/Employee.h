#pragma once
#include <iostream>
#include <string>

using namespace std;
class Employee
{
protected:
	int ID;
	string Fio;
	double payment;
	int time = 40;
public:
	int getID() { return ID; }
	int getTime() { return time; }
	double getPay() { return payment; }
	string getFIO() { return Fio; }
	virtual void CalcPay(){}
	virtual string Outspec() { return" "; }
	Employee() {}
	~Employee() {}
};

